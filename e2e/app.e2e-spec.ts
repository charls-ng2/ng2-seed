import { CharlasPage } from './app.po';

describe('charlas App', function() {
  let page: CharlasPage;

  beforeEach(() => {
    page = new CharlasPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

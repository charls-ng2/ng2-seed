import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  private todos = [
    {
      name: 'gi',
      description : 'sdfghj',
      eta : new Date(),
      id: 0
    },
    {
      name: 'gidsg',
      description : 'sdfghjsdf',
      eta : new Date(),
      id: 2
    }
  ]

  constructor() { }

  ngOnInit() {
  }

  save(todo){
    this.todos.push(todo);
  }

}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {

  private newTodo = {
  };

  @Output('create') create =  new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  private submit(){
    this.create.emit(this.newTodo);
    this.newTodo = {};
  }

}
